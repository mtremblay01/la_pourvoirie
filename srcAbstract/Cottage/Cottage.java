package Cottage;

public abstract class Cottage implements CottageBase{

	private int days;
	private double unitPrice;
	
	public Cottage(int days, double unitPrice) {
		this.days = days;
		this.unitPrice = unitPrice;
	}
	
	public double getBasePrice() {
		return this.unitPrice;
	}
	
	public double getPrice() {
		return this.unitPrice * this.days;
	}
	
	public double getTotalPrice() {
		return getPrice();
	}
}
