package Cottage;

public interface CottageBase {
	public double getBasePrice();
	public double getPrice();
	public double getTotalPrice();
}
