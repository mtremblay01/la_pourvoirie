package Decorater;

import Cottage.CottageBase;

public abstract class IndividualServices implements CottageBase {

	private CottageBase component;
	private double unitPrice;
	private int nbPerson;
	
	protected IndividualServices(CottageBase component, double unitPrice, int nbPerson) {
		validateNbPerson(nbPerson);
		this.nbPerson = nbPerson;
		this.component = component;
		this.unitPrice = unitPrice;
	}
	public double getBasePrice(){
		return this.unitPrice;
	}
	
	public double getTotalPrice(){
		return this.getPrice() + this.component.getTotalPrice();
	}
	
	public double getUnitPrice(){
		return this.unitPrice;
	}
	
	public void setUnitPrice(double unitPrice){
		this.unitPrice = unitPrice;
	}

	public double getPrice() {
		return this.nbPerson * this.getBasePrice();
	}
	
	protected abstract void validateNbPerson(int nbPerson);
}
