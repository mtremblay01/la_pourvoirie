package Decorater;
import Cottage.CottageBase;

public abstract class CottageDecorater implements CottageBase{
	
	private CottageBase component;
	private double unitPrice;
	
	protected CottageDecorater(CottageBase component, double unitPrice){
		this.component = component;
		this.unitPrice = unitPrice;
	}
	
	public double getBasePrice(){
		return this.unitPrice;
	}
	
	public double getTotalPrice(){
		return this.getPrice() + this.component.getTotalPrice();
	}
	
	public double getUnitPrice(){
		return this.unitPrice;
	}
	
	public void setUnitPrice(double unitPrice){
		this.unitPrice = unitPrice;
	}
}
