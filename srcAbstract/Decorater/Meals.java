package Decorater;

import Cottage.CottageBase;

public abstract class Meals extends CottageDecorater{

	private int nbDays;
	private int nbPersons;
	
	protected Meals(CottageBase component, double unitPrice, int nbDays, int nbPersons) {
		super(component, unitPrice);
		this.nbDays = nbDays;
		this.nbPersons = nbPersons;
	}

	public double getPrice(){
		return super.getBasePrice() * this.nbDays * this.nbPersons;
				
	}
}
