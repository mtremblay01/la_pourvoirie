package Decorater;

import Cottage.CottageBase;

public abstract class Transport extends CottageDecorater {

	private int noPerson;
	
	protected Transport(CottageBase component, double unitPrice, int noPerson) {
		super(component, unitPrice);
		this.noPerson = noPerson;
	}

	@Override
	public double getPrice() {
		return getBasePrice() * noPerson;
	}

}
