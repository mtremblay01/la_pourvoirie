package Decorater;

import Cottage.CottageBase;

public abstract class DailyServices implements CottageBase {

	private CottageBase component;
	private double unitPrice;
	private int nbDays;
	
	protected DailyServices(CottageBase component, double unitPrice, int nbDays) {
		this.component = component;
		this.unitPrice = unitPrice;
		this.nbDays = nbDays;
	}
	
	public double getBasePrice(){
		return this.unitPrice;
	}
	
	public double getTotalPrice(){
		return this.getPrice() + this.component.getTotalPrice();
	}
	
	public double getUnitPrice(){
		return this.unitPrice;
	}
	
	public void setUnitPrice(double unitPrice){
		this.unitPrice = unitPrice;
	}
	
	public double getPrice() {
		return this.nbDays * this.getBasePrice();
	}
}
