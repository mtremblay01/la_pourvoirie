package Mocks;

import Cottage.CottageBase;

public class CottageMock implements CottageBase{

	@Override
	public double getBasePrice() {
		return 0;
	}

	@Override
	public double getPrice() {
		return 0;
	}

	@Override
	public double getTotalPrice() {
		return 0;
	}
}
