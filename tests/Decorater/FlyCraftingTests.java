package Decorater;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import Mocks.CottageMock;
import exceptions.InvalidPersonNumberException;

public class FlyCraftingTests {

	public static final int ANY_NB_PERSONS = 4;
	private FlyCrafting flyCrafting;
	private CottageMock cottage;
	
	@Before
	public void createTrapping(){
		this.cottage = new CottageMock();
		this.flyCrafting = new FlyCrafting(this.cottage, ANY_NB_PERSONS);
	}

	@Test
	public void getUnitPrice_ShouldReturnUnitPriceForFlyCrafting(){
		//Act
		double unitPrice = flyCrafting.getUnitPrice();
		
		//Assert
		final double EXPECTED_UNIT_PRICE = flyCrafting.getUnitPrice();
		assertEquals(EXPECTED_UNIT_PRICE, unitPrice, 0.00);
	}
	
	@Test
	public void setUnitPrice_shouldChangeUnitPriceForTheFlyCrafting(){
		//Arrange
		final double ANY_UNIT_PRICE = 10;
		
		//Act
		flyCrafting.setUnitPrice(ANY_UNIT_PRICE);
		
		//Assert
		double unitPrice = flyCrafting.getUnitPrice();
		assertEquals(ANY_UNIT_PRICE, unitPrice, 0.0);
	}
	
	@Test
	public void getBasePrice_shouldReturnBasePriceForFlyCrafting(){
		//Act
		double basePrice = this.flyCrafting.getBasePrice();
		
		//Assert
		final double EXPECTED_PRICE = flyCrafting.getUnitPrice();
		assertEquals(EXPECTED_PRICE, basePrice, 0.0);
		
	}
	
	@Test
	public void getPrice_shouldReturnPriceForFlyCraftingConsideringNbPersons(){
		//Act
		double price = this.flyCrafting.getPrice();
		
		//Assert
		final double EXPECTED_PRICE = flyCrafting.getUnitPrice() * ANY_NB_PERSONS;
		assertEquals(EXPECTED_PRICE, price, 0.0);
	}
	
	@Test
	public void getTotalPrice_shouldReturnPriceForFlyCraftingConsideringNbPersonsAndOtherDecorations(){
		//Arrange
		FlyCrafting flyCrafting2 = new FlyCrafting(this.flyCrafting, ANY_NB_PERSONS);
		
		//Act
		double totalPrice = flyCrafting2.getTotalPrice();
		
		//Assert
		final double EXPECTED_PRICE = (flyCrafting.getUnitPrice() * ANY_NB_PERSONS) * 2;
		assertEquals(EXPECTED_PRICE, totalPrice, 0.0);
	}
	
	@Test (expected = InvalidPersonNumberException.class)
	public void createFlyCraftingObservation_shouldThrowAnExceptionIfNbPersonBelowMinimum(){
		
		//Act
		FlyCrafting flyCrafting = new FlyCrafting(this.cottage, BearObservation.MINIMUM_NB_PERSON -1);
	}
}
