package Decorater;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import Mocks.CottageMock;

public class BreakfastAndLunchTests {

	public static final int ANY_NB_DAYS = 5;
	public static final int ANY_NB_PERSONS = 4;
	private BreakfastAndLunch breakfastAndLunch;
	private CottageMock cottage;
	
	@Before
	public void createBreakfastAndLunch(){
		this.cottage = new CottageMock();
		this.breakfastAndLunch = new BreakfastAndLunch(this.cottage, ANY_NB_DAYS, ANY_NB_PERSONS);
	}

	@Test
	public void getUnitPrice_ShouldReturnUnitPriceForTheBreakfastAndLunchService(){
		//Act
		double unitPrice = breakfastAndLunch.getUnitPrice();
		
		//Assert
		final double EXPECTED_UNIT_PRICE = breakfastAndLunch.getUnitPrice();
		assertEquals(EXPECTED_UNIT_PRICE, unitPrice, 0.00);
	}
	
	@Test
	public void setUnitPrice_shouldChangeUnitPriceForTheBreakfastAndLunchService(){
		//Arrange
		final double ANY_UNIT_PRICE = 10;
		
		//Act
		breakfastAndLunch.setUnitPrice(ANY_UNIT_PRICE);
		
		//Assert
		double unitPrice = breakfastAndLunch.getUnitPrice();
		assertEquals(ANY_UNIT_PRICE, unitPrice, 0.0);
	}
	
	@Test
	public void getBasePrice_shouldReturnBasePriceForBreakfastAndLunchService(){
		//Act
		double basePrice = this.breakfastAndLunch.getBasePrice();
		
		//Assert
		final double EXPECTED_PRICE = breakfastAndLunch.getUnitPrice();
		assertEquals(EXPECTED_PRICE, basePrice, 0.0);
		
	}
	
	@Test
	public void getPrice_shouldReturnPriceForBreakfastAndLunchServiceConsideringNbDays(){
		//Act
		double price = this.breakfastAndLunch.getPrice();
		
		//Assert
		final double EXPECTED_PRICE = breakfastAndLunch.getUnitPrice() * ANY_NB_DAYS * ANY_NB_PERSONS;
		assertEquals(EXPECTED_PRICE, price, 0.0);
	}
	
	@Test
	public void getTotalPrice_shouldReturnPriceForBreakfastAndLunchServiceConsideringNbDaysAndOtherDecorations(){
		//Arrange
		BreakfastAndLunch breakfastAndLunch2 = new BreakfastAndLunch(this.breakfastAndLunch, ANY_NB_DAYS, ANY_NB_PERSONS);
		
		//Act
		double totalPrice = breakfastAndLunch2.getTotalPrice();
		
		//Assert
		final double EXPECTED_PRICE = (breakfastAndLunch.getUnitPrice() * ANY_NB_DAYS * ANY_NB_PERSONS) * 2;
		assertEquals(EXPECTED_PRICE, totalPrice, 0.0);
	}
}
