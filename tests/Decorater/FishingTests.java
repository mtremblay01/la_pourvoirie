package Decorater;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import Mocks.CottageMock;
import exceptions.InvalidPersonNumberException;

public class FishingTests {

	public static final int ANY_NB_PERSONS = 4;
	private Fishing Fishing;
	private CottageMock cottage;
	
	@Before
	public void createTrapping(){
		this.cottage = new CottageMock();
		this.Fishing = new Fishing(this.cottage, ANY_NB_PERSONS);
	}

	@Test
	public void getUnitPrice_ShouldReturnUnitPriceForFishing(){
		//Act
		double unitPrice = Fishing.getUnitPrice();
		
		//Assert
		final double EXPECTED_UNIT_PRICE = Fishing.getUnitPrice();
		assertEquals(EXPECTED_UNIT_PRICE, unitPrice, 0.00);
	}
	
	@Test
	public void setUnitPrice_shouldChangeUnitPriceForTheFishing(){
		//Arrange
		final double ANY_UNIT_PRICE = 10;
		
		//Act
		Fishing.setUnitPrice(ANY_UNIT_PRICE);
		
		//Assert
		double unitPrice = Fishing.getUnitPrice();
		assertEquals(ANY_UNIT_PRICE, unitPrice, 0.0);
	}
	
	@Test
	public void getBasePrice_shouldReturnBasePriceForFishing(){
		//Act
		double basePrice = this.Fishing.getBasePrice();
		
		//Assert
		final double EXPECTED_PRICE = Fishing.getUnitPrice();
		assertEquals(EXPECTED_PRICE, basePrice, 0.0);
		
	}
	
	@Test
	public void getPrice_shouldReturnPriceForFishingConsideringNbPersons(){
		//Act
		double price = this.Fishing.getPrice();
		
		//Assert
		final double EXPECTED_PRICE = Fishing.getUnitPrice() * ANY_NB_PERSONS;
		assertEquals(EXPECTED_PRICE, price, 0.0);
	}
	
	@Test
	public void getTotalPrice_shouldReturnPriceForFishingConsideringNbPersonsAndOtherDecorations(){
		//Arrange
		Fishing Fishing2 = new Fishing(this.Fishing, ANY_NB_PERSONS);
		
		//Act
		double totalPrice = Fishing2.getTotalPrice();
		
		//Assert
		final double EXPECTED_PRICE = (Fishing.getUnitPrice() * ANY_NB_PERSONS) * 2;
		assertEquals(EXPECTED_PRICE, totalPrice, 0.0);
	}
	
	@Test (expected = InvalidPersonNumberException.class)
	public void createFishingObservation_shouldThrowAnExceptionIfNbPersonBelowMinimum(){
		
		//Act
		Fishing fishing = new Fishing(this.cottage, Fishing.MINIMUM_PERSON -1);
	}
	
	@Test (expected = InvalidPersonNumberException.class)
	public void createFishingObservation_shouldThrowAnExceptionIfNbPersonOverMmaximum(){
		
		//Act
		Fishing fishing = new Fishing(this.cottage, Fishing.MAXIMUM_PERSON+1);
	}

}
