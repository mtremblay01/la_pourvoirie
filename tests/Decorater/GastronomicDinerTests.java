package Decorater;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import Mocks.CottageMock;

public class GastronomicDinerTests {

	public static final int ANY_NB_DAYS = 5;
	public static final int ANY_NB_PERSONS = 4;
	private GastronomicDinner gastronomicDiner;
	private CottageMock cottage;
	
	@Before
	public void createGastronomicDiner(){
		this.cottage = new CottageMock();
		this.gastronomicDiner = new GastronomicDinner(this.cottage, ANY_NB_DAYS, ANY_NB_PERSONS);
	}

	@Test
	public void getUnitPrice_ShouldReturnUnitPriceForGastronomicDiner(){
		//Act
		double unitPrice = gastronomicDiner.getUnitPrice();
		
		//Assert
		final double EXPECTED_UNIT_PRICE = gastronomicDiner.getUnitPrice();
		assertEquals(EXPECTED_UNIT_PRICE, unitPrice, 0.00);
	}
	
	@Test
	public void setUnitPrice_shouldChangeUnitPriceForTheGastronomicDiner(){
		//Arrange
		final double ANY_UNIT_PRICE = 10;
		
		//Act
		gastronomicDiner.setUnitPrice(ANY_UNIT_PRICE);
		
		//Assert
		double unitPrice = gastronomicDiner.getUnitPrice();
		assertEquals(ANY_UNIT_PRICE, unitPrice, 0.0);
	}
	
	@Test
	public void getBasePrice_shouldReturnBasePriceForGastronomicDiner(){
		//Act
		double basePrice = this.gastronomicDiner.getBasePrice();
		
		//Assert
		final double EXPECTED_PRICE = gastronomicDiner.getUnitPrice();
		assertEquals(EXPECTED_PRICE, basePrice, 0.0);
		
	}
	
	@Test
	public void getPrice_shouldReturnPriceForGastronomicDinerConsideringNbDays(){
		//Act
		double price = this.gastronomicDiner.getPrice();
		
		//Assert
		final double EXPECTED_PRICE = gastronomicDiner.getUnitPrice() * ANY_NB_DAYS * ANY_NB_PERSONS;
		assertEquals(EXPECTED_PRICE, price, 0.0);
	}
	
	@Test
	public void getTotalPrice_shouldReturnPriceForGastronomicDinerConsideringNbDaysAndOtherDecorations(){
		//Arrange
		GastronomicDinner gastronomicDiner = new GastronomicDinner(this.gastronomicDiner, ANY_NB_DAYS, ANY_NB_PERSONS);
		
		//Act
		double totalPrice = gastronomicDiner.getTotalPrice();
		
		//Assert
		final double EXPECTED_PRICE = (gastronomicDiner.getUnitPrice() * ANY_NB_DAYS * ANY_NB_PERSONS) * 2;
		assertEquals(EXPECTED_PRICE, totalPrice, 0.0);
	}

}
