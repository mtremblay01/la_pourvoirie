package Decorater;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import Mocks.CottageMock;

public class DinerTests {

	public static final int ANY_NB_DAYS = 5;
	public static final int ANY_NB_PERSONS = 4;
	private Dinner diner;
	private CottageMock cottage;
	
	@Before
	public void createDiner(){
		this.cottage = new CottageMock();
		this.diner = new Dinner(this.cottage, ANY_NB_DAYS, ANY_NB_PERSONS);
	}

	@Test
	public void getUnitPrice_ShouldReturnUnitPriceForDiner(){
		//Act
		double unitPrice = diner.getUnitPrice();
		
		//Assert
		final double EXPECTED_UNIT_PRICE = diner.getUnitPrice();
		assertEquals(EXPECTED_UNIT_PRICE, unitPrice, 0.00);
	}
	
	@Test
	public void setUnitPrice_shouldChangeUnitPriceForTheDiner(){
		//Arrange
		final double ANY_UNIT_PRICE = 10;
		
		//Act
		diner.setUnitPrice(ANY_UNIT_PRICE);
		
		//Assert
		double unitPrice = diner.getUnitPrice();
		assertEquals(ANY_UNIT_PRICE, unitPrice, 0.0);
	}
	
	@Test
	public void getBasePrice_shouldReturnBasePriceForDiner(){
		//Act
		double basePrice = this.diner.getBasePrice();
		
		//Assert
		final double EXPECTED_PRICE = diner.getUnitPrice();
		assertEquals(EXPECTED_PRICE, basePrice, 0.0);
		
	}
	
	@Test
	public void getPrice_shouldReturnPriceForDinerConsideringNbDays(){
		//Act
		double price = this.diner.getPrice();
		
		//Assert
		final double EXPECTED_PRICE = diner.getUnitPrice() * ANY_NB_DAYS * ANY_NB_PERSONS;
		assertEquals(EXPECTED_PRICE, price, 0.0);
	}
	
	@Test
	public void getTotalPrice_shouldReturnPriceForDinerConsideringNbDaysAndOtherDecorations(){
		//Arrange
		Dinner diner2 = new Dinner(this.diner, ANY_NB_DAYS, ANY_NB_PERSONS);
		
		//Act
		double totalPrice = diner2.getTotalPrice();
		
		//Assert
		final double EXPECTED_PRICE = (diner.getUnitPrice() * ANY_NB_DAYS * ANY_NB_PERSONS) * 2;
		assertEquals(EXPECTED_PRICE, totalPrice, 0.0);
	}
}
