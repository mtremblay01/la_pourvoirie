package Decorater;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import Mocks.CottageMock;
import exceptions.InvalidPersonNumberException;

public class FlyFishingTests {

	public static final int ANY_NB_PERSONS = 4;
	private FlyFishing flyFishing;
	private CottageMock cottage;
	
	@Before
	public void createTrapping(){
		this.cottage = new CottageMock();
		this.flyFishing = new FlyFishing(this.cottage, ANY_NB_PERSONS);
	}

	@Test
	public void getUnitPrice_ShouldReturnUnitPriceForFlyFishing(){
		//Act
		double unitPrice = flyFishing.getUnitPrice();
		
		//Assert
		final double EXPECTED_UNIT_PRICE = flyFishing.getUnitPrice();
		assertEquals(EXPECTED_UNIT_PRICE, unitPrice, 0.00);
	}
	
	@Test
	public void setUnitPrice_shouldChangeUnitPriceForTheFlyFishing(){
		//Arrange
		final double ANY_UNIT_PRICE = 10;
		
		//Act
		flyFishing.setUnitPrice(ANY_UNIT_PRICE);
		
		//Assert
		double unitPrice = flyFishing.getUnitPrice();
		assertEquals(ANY_UNIT_PRICE, unitPrice, 0.0);
	}
	
	@Test
	public void getBasePrice_shouldReturnBasePriceForFlyFishing(){
		//Act
		double basePrice = this.flyFishing.getBasePrice();
		
		//Assert
		final double EXPECTED_PRICE = flyFishing.getUnitPrice();
		assertEquals(EXPECTED_PRICE, basePrice, 0.0);
		
	}
	
	@Test
	public void getPrice_shouldReturnPriceForFlyFishingConsideringNbPersons(){
		//Act
		double price = this.flyFishing.getPrice();
		
		//Assert
		final double EXPECTED_PRICE = flyFishing.getUnitPrice() * ANY_NB_PERSONS;
		assertEquals(EXPECTED_PRICE, price, 0.0);
	}
	
	@Test
	public void getTotalPrice_shouldReturnPriceForFlyFishingConsideringNbPersonsAndOtherDecorations(){
		//Arrange
		FlyFishing flyFishing2 = new FlyFishing(this.flyFishing, ANY_NB_PERSONS);
		
		//Act
		double totalPrice = flyFishing2.getTotalPrice();
		
		//Assert
		final double EXPECTED_PRICE = (flyFishing.getUnitPrice() * ANY_NB_PERSONS) * 2;
		assertEquals(EXPECTED_PRICE, totalPrice, 0.0);
	}
	
	@Test (expected = InvalidPersonNumberException.class)
	public void createFlyFishingObservation_shouldThrowAnExceptionIfNbPersonBelowMinimum(){
		
		//Act
		FlyFishing flyFishing = new FlyFishing(this.cottage, FlyFishing.MINIMUM_NB_PERSON -1);
	}
}
