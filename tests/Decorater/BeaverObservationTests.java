package Decorater;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import Mocks.CottageMock;
import exceptions.InvalidPersonNumberException;

public class BeaverObservationTests {

	public static final int ANY_NB_PERSON = 5;
	private BeaverObservation BeaverObservation;
	private CottageMock cottage;
	
	@Before
	public void createCleaning(){
		this.cottage = new CottageMock();
		this.BeaverObservation = new BeaverObservation(this.cottage, ANY_NB_PERSON);
	}

	@Test
	public void getUnitPrice_ShouldReturnUnitPriceForTheBeaverObservation(){
		//Act
		double unitPrice = BeaverObservation.getUnitPrice();
		
		//Assert
		final double EXPECTED_UNIT_PRICE = BeaverObservation.DEFAULT_UNIT_PRICE;
		assertEquals(EXPECTED_UNIT_PRICE, unitPrice, 0.00);
	}
	
	@Test
	public void setUnitPrice_shouldChangeUnitPriceForTheBeaverObservation(){
		//Arrange
		final double ANY_UNIT_PRICE = 10;
		
		//Act
		BeaverObservation.setUnitPrice(ANY_UNIT_PRICE);
		
		//Assert
		double unitPrice = BeaverObservation.getUnitPrice();
		assertEquals(ANY_UNIT_PRICE, unitPrice, 0.0);
	}
	
	@Test
	public void getBasePrice_shouldReturnBasePriceForBeaverObservation(){
		//Act
		double basePrice = this.BeaverObservation.getBasePrice();
		
		//Assert
		final double EXPECTED_PRICE = BeaverObservation.getUnitPrice();
		assertEquals(EXPECTED_PRICE, basePrice, 0.0);
		
	}
	
	@Test
	public void getPrice_shouldReturnPriceForBeaverObservationServiceConsideringNbPerson(){
		//Act
		double price = this.BeaverObservation.getPrice();
		
		//Assert
		final double EXPECTED_PRICE = BeaverObservation.getUnitPrice() * ANY_NB_PERSON;
		assertEquals(EXPECTED_PRICE, price, 0.0);
	}
	
	@Test
	public void getTotalPrice_shouldReturnPriceForBeaverObservationServiceConsideringNbPersonAndOtherDecorations(){
		//Arrange
		BeaverObservation beaver = new BeaverObservation(this.BeaverObservation, ANY_NB_PERSON);
		
		//Act
		double totalPrice = beaver.getTotalPrice();
		
		//Assert
		final double EXPECTED_PRICE = (BeaverObservation.getUnitPrice() * ANY_NB_PERSON) * 2;
		assertEquals(EXPECTED_PRICE, totalPrice, 0.0);
	}

	@Test (expected = InvalidPersonNumberException.class)
	public void createBeaverObservation_shouldThrowAnExceptionIfNbPersonBelowMinimum(){
		
		//Act
		BeaverObservation bear = new BeaverObservation(this.BeaverObservation, BeaverObservation.MINIMUM_NB_PERSON -1);
	}
}
