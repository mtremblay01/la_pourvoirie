package Decorater;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import Mocks.CottageMock;

public class CleaningTests {

	public static final int ANY_NB_DAYS = 5;
	private Cleaning cleaning;
	private CottageMock cottage;
	
	@Before
	public void createCleaning(){
		this.cottage = new CottageMock();
		this.cleaning = new Cleaning(this.cottage, ANY_NB_DAYS);
	}

	@Test
	public void getUnitPrice_ShouldReturnUnitPriceForTheCleaningService(){
		//Act
		double unitPrice = cleaning.getUnitPrice();
		
		//Assert
		final double EXPECTED_UNIT_PRICE = cleaning.getUnitPrice();
		assertEquals(EXPECTED_UNIT_PRICE, unitPrice, 0.00);
	}
	
	@Test
	public void setUnitPrice_shouldChangeUnitPriceForTheCleaningService(){
		//Arrange
		final double ANY_UNIT_PRICE = 10;
		
		//Act
		cleaning.setUnitPrice(ANY_UNIT_PRICE);
		
		//Assert
		double unitPrice = cleaning.getUnitPrice();
		assertEquals(ANY_UNIT_PRICE, unitPrice, 0.0);
	}
	
	@Test
	public void getBasePrice_shouldReturnBasePriceForCleaningService(){
		//Act
		double basePrice = this.cleaning.getBasePrice();
		
		//Assert
		final double EXPECTED_PRICE = cleaning.DEFAULT_UNIT_PRICE;
		assertEquals(EXPECTED_PRICE, basePrice, 0.0);
		
	}
	
	@Test
	public void getPrice_shouldReturnPriceForCleaningServiceConsideringNbDays(){
		//Act
		double price = this.cleaning.getPrice();
		
		//Assert
		final double EXPECTED_PRICE = cleaning.getUnitPrice() * ANY_NB_DAYS;
		assertEquals(EXPECTED_PRICE, price, 0.0);
	}
	
	@Test
	public void getTotalPrice_shouldReturnPriceForCleaningServiceConsideringNbDaysAndOtherDecorations(){
		//Arrange
		Cleaning cleaning2 = new Cleaning(this.cleaning, ANY_NB_DAYS);
		
		//Act
		double totalPrice = cleaning2.getTotalPrice();
		
		//Assert
		final double EXPECTED_PRICE = (cleaning.getUnitPrice() * ANY_NB_DAYS) * 2;
		assertEquals(EXPECTED_PRICE, totalPrice, 0.0);
	}
}

