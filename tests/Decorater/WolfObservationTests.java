package Decorater;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import Mocks.CottageMock;
import exceptions.InvalidPersonNumberException;

public class WolfObservationTests {

	public static final int ANY_NB_PERSON = 5;
	private WolfObservation WolfObservation;
	private CottageMock cottage;
	
	@Before
	public void createCleaning(){
		this.cottage = new CottageMock();
		this.WolfObservation = new WolfObservation(this.cottage, ANY_NB_PERSON);
	}

	@Test
	public void getUnitPrice_ShouldReturnUnitPriceForTheWolfObservation(){
		//Act
		double unitPrice = WolfObservation.getUnitPrice();
		
		//Assert
		final double EXPECTED_UNIT_PRICE = WolfObservation.DEFAULT_UNIT_PRICE;
		assertEquals(EXPECTED_UNIT_PRICE, unitPrice, 0.00);
	}
	
	@Test
	public void setUnitPrice_shouldChangeUnitPriceForTheWolfObservation(){
		//Arrange
		final double ANY_UNIT_PRICE = 10;
		
		//Act
		WolfObservation.setUnitPrice(ANY_UNIT_PRICE);
		
		//Assert
		double unitPrice = WolfObservation.getUnitPrice();
		assertEquals(ANY_UNIT_PRICE, unitPrice, 0.0);
	}
	
	@Test
	public void getBasePrice_shouldReturnBasePriceForWolfObservation(){
		//Act
		double basePrice = this.WolfObservation.getBasePrice();
		
		//Assert
		final double EXPECTED_PRICE = WolfObservation.getUnitPrice();
		assertEquals(EXPECTED_PRICE, basePrice, 0.0);
		
	}
	
	@Test
	public void getPrice_shouldReturnPriceForWolfObservationServiceConsideringNbPerson(){
		//Act
		double price = this.WolfObservation.getPrice();
		
		//Assert
		final double EXPECTED_PRICE = WolfObservation.getUnitPrice() * ANY_NB_PERSON;
		assertEquals(EXPECTED_PRICE, price, 0.0);
	}
	
	@Test
	public void getTotalPrice_shouldReturnPriceForWolfObservationServiceConsideringNbPersonAndOtherDecorations(){
		//Arrange
		WolfObservation wolf = new WolfObservation(this.WolfObservation, ANY_NB_PERSON);
		
		//Act
		double totalPrice = wolf.getTotalPrice();
		
		//Assert
		final double EXPECTED_PRICE = (WolfObservation.getUnitPrice() * ANY_NB_PERSON) * 2;
		assertEquals(EXPECTED_PRICE, totalPrice, 0.0);
	}
	
	@Test (expected = InvalidPersonNumberException.class)
	public void createBeaverObservation_shouldThrowAnExceptionIfNbPersonBelowMinimum(){
		
		//Act
		WolfObservation bear = new WolfObservation(this.WolfObservation, WolfObservation.MINIMUM_NB_PERSON -1);
	}

}
