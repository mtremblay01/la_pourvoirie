package Decorater;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import Mocks.CottageMock;
import exceptions.InvalidPersonNumberException;

public class HuntingTests {

	public static final int ANY_NB_PERSONS = 4;
	private Hunting Hunting;
	private CottageMock cottage;
	
	@Before
	public void createTrapping(){
		this.cottage = new CottageMock();
		this.Hunting = new Hunting(this.cottage, ANY_NB_PERSONS);
	}

	@Test
	public void getUnitPrice_ShouldReturnUnitPriceForHunting(){
		//Act
		double unitPrice = Hunting.getUnitPrice();
		
		//Assert
		final double EXPECTED_UNIT_PRICE = Hunting.getUnitPrice();
		assertEquals(EXPECTED_UNIT_PRICE, unitPrice, 0.00);
	}
	
	@Test
	public void setUnitPrice_shouldChangeUnitPriceForTheHunting(){
		//Arrange
		final double ANY_UNIT_PRICE = 10;
		
		//Act
		Hunting.setUnitPrice(ANY_UNIT_PRICE);
		
		//Assert
		double unitPrice = Hunting.getUnitPrice();
		assertEquals(ANY_UNIT_PRICE, unitPrice, 0.0);
	}
	
	@Test
	public void getBasePrice_shouldReturnBasePriceForHunting(){
		//Act
		double basePrice = this.Hunting.getBasePrice();
		
		//Assert
		final double EXPECTED_PRICE = Hunting.getUnitPrice();
		assertEquals(EXPECTED_PRICE, basePrice, 0.0);
		
	}
	
	@Test
	public void getPrice_shouldReturnPriceForHuntingConsideringNbPersons(){
		//Act
		double price = this.Hunting.getPrice();
		
		//Assert
		final double EXPECTED_PRICE = Hunting.getUnitPrice() * ANY_NB_PERSONS;
		assertEquals(EXPECTED_PRICE, price, 0.0);
	}
	
	@Test
	public void getTotalPrice_shouldReturnPriceForHuntingConsideringNbPersonsAndOtherDecorations(){
		//Arrange
		Hunting Hunting2 = new Hunting(this.Hunting, ANY_NB_PERSONS);
		
		//Act
		double totalPrice = Hunting2.getTotalPrice();
		
		//Assert
		final double EXPECTED_PRICE = (Hunting.getUnitPrice() * ANY_NB_PERSONS) * 2;
		assertEquals(EXPECTED_PRICE, totalPrice, 0.0);
	}
	
	@Test (expected = InvalidPersonNumberException.class)
	public void createHuntingObservation_shouldThrowAnExceptionIfNbPersonBelowMinimum(){
		
		//Act
		Hunting fishing = new Hunting(this.cottage, Hunting.MINIMUM_PERSON -1);
	}
	
	@Test (expected = InvalidPersonNumberException.class)
	public void createHuntingObservation_shouldThrowAnExceptionIfNbPersonOverMmaximum(){
		
		//Act
		Hunting fishing = new Hunting(this.cottage, Hunting.MAXIMUM_PERSON+1);
	}


}
