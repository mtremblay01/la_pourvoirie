package Decorater;

import static org.junit.Assert.*;

import org.junit.Test;

import Mocks.CottageMock;

public class TransportTests {

	@Test
	public void GIVEN_Boat_WHEN_TotalPriceIsAsked_THEN_PriceIsReturned() {
		int noPerson = 3;
		CottageMock cottage = new CottageMock();
		Boat boat = new Boat(cottage, noPerson);
		
		assertEquals(boat.getTotalPrice(), noPerson * Boat.DEFAULT_UNIT_PRICE, 0.0);
	}
	
	@Test
	public void GIVEN_Boat_WHEN_PriceIsAsked_THEN_PriceIsReturned() {
		int noPerson = 3;
		CottageMock cottage = new CottageMock();
		Boat boat = new Boat(cottage, noPerson);
		
		assertEquals(boat.getPrice(), noPerson * Boat.DEFAULT_UNIT_PRICE, 0.0);
	}

	@Test
	public void GIVEN_Boat_WHEN_BasePriceIsAsked_THEN_PriceIsReturned() {
		int noPerson = 3;
		CottageMock cottage = new CottageMock();
		Boat boat = new Boat(cottage, noPerson);
		
		assertEquals(boat.getBasePrice(), Boat.DEFAULT_UNIT_PRICE, 0.0);
	}

	@Test
	public void GIVEN_HydroAirplane_WHEN_TotalPriceIsAsked_THEN_PriceIsReturned() {
		int noPerson = 3;
		CottageMock cottage = new CottageMock();
		HydroAirplane hydroAirplane = new HydroAirplane(cottage, noPerson);
		
		assertEquals(hydroAirplane.getTotalPrice(), noPerson * HydroAirplane.DEFAULT_UNIT_PRICE, 0.0);
	}
	
	@Test
	public void GIVEN_HydroAirplane_WHEN_PriceIsAsked_THEN_PriceIsReturned() {
		int noPerson = 3;
		CottageMock cottage = new CottageMock();
		HydroAirplane hydroAirplane = new HydroAirplane(cottage, noPerson);
		
		assertEquals(hydroAirplane.getPrice(), noPerson * HydroAirplane.DEFAULT_UNIT_PRICE, 0.0);
	}

	@Test
	public void GIVEN_HydroAirplane_WHEN_BasePriceIsAsked_THEN_PriceIsReturned() {
		int noPerson = 3;
		CottageMock cottage = new CottageMock();
		HydroAirplane hydroAirplane = new HydroAirplane(cottage, noPerson);
		
		assertEquals(hydroAirplane.getBasePrice(), HydroAirplane.DEFAULT_UNIT_PRICE, 0.0);
	}
}
