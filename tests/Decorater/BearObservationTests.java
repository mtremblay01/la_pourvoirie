package Decorater;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import Mocks.CottageMock;
import exceptions.InvalidPersonNumberException;

public class BearObservationTests {

	public static final int ANY_NB_PERSON = 5;
	private BearObservation BearObservation;
	private CottageMock cottage;
	
	@Before
	public void createCleaning(){
		this.cottage = new CottageMock();
		this.BearObservation = new BearObservation(this.cottage, ANY_NB_PERSON);
	}

	@Test
	public void getUnitPrice_ShouldReturnUnitPriceForTheBearObservation(){
		//Act
		double unitPrice = BearObservation.getUnitPrice();
		
		//Assert
		final double EXPECTED_UNIT_PRICE = BearObservation.DEFAULT_UNIT_PRICE;
		assertEquals(EXPECTED_UNIT_PRICE, unitPrice, 0.00);
	}
	
	@Test
	public void setUnitPrice_shouldChangeUnitPriceForTheBearObservation(){
		//Arrange
		final double ANY_UNIT_PRICE = 10;
		
		//Act
		BearObservation.setUnitPrice(ANY_UNIT_PRICE);
		
		//Assert
		double unitPrice = BearObservation.getUnitPrice();
		assertEquals(ANY_UNIT_PRICE, unitPrice, 0.0);
	}
	
	@Test
	public void getBasePrice_shouldReturnBasePriceForBearObservation(){
		//Act
		double basePrice = this.BearObservation.getBasePrice();
		
		//Assert
		final double EXPECTED_PRICE = BearObservation.getUnitPrice();
		assertEquals(EXPECTED_PRICE, basePrice, 0.0);
		
	}
	
	@Test
	public void getPrice_shouldReturnPriceForBearObservationServiceConsideringNbPerson(){
		//Act
		double price = this.BearObservation.getPrice();
		
		//Assert
		final double EXPECTED_PRICE = BearObservation.getUnitPrice() * ANY_NB_PERSON;
		assertEquals(EXPECTED_PRICE, price, 0.0);
	}
	
	@Test
	public void getTotalPrice_shouldReturnPriceForBearObservationServiceConsideringNbPersonAndOtherDecorations(){
		//Arrange
		BearObservation bear = new BearObservation(this.BearObservation, ANY_NB_PERSON);
		
		//Act
		double totalPrice = bear.getTotalPrice();
		
		//Assert
		final double EXPECTED_PRICE = (BearObservation.getUnitPrice() * ANY_NB_PERSON) * 2;
		assertEquals(EXPECTED_PRICE, totalPrice, 0.0);
	}
	
	@Test (expected = InvalidPersonNumberException.class)
	public void createBearObservation_shouldThrowAnExceptionIfNbPersonBelowMinimum(){
		
		//Act
		BearObservation bear = new BearObservation(this.BearObservation, BearObservation.MINIMUM_NB_PERSON -1);
	}
}
