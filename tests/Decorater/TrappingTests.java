package Decorater;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import Mocks.CottageMock;
import exceptions.InvalidPersonNumberException;

public class TrappingTests {

	public static final int ANY_NB_PERSONS = 4;
	private Trapping trapping;
	private CottageMock cottage;
	
	@Before
	public void createTrapping(){
		this.cottage = new CottageMock();
		this.trapping = new Trapping(this.cottage, ANY_NB_PERSONS);
	}

	@Test
	public void getUnitPrice_ShouldReturnUnitPriceForTrapping(){
		//Act
		double unitPrice = trapping.getUnitPrice();
		
		//Assert
		final double EXPECTED_UNIT_PRICE = trapping.getUnitPrice();
		assertEquals(EXPECTED_UNIT_PRICE, unitPrice, 0.00);
	}
	
	@Test
	public void setUnitPrice_shouldChangeUnitPriceForTheTrapping(){
		//Arrange
		final double ANY_UNIT_PRICE = 10;
		
		//Act
		trapping.setUnitPrice(ANY_UNIT_PRICE);
		
		//Assert
		double unitPrice = trapping.getUnitPrice();
		assertEquals(ANY_UNIT_PRICE, unitPrice, 0.0);
	}
	
	@Test
	public void getBasePrice_shouldReturnBasePriceForTrapping(){
		//Act
		double basePrice = this.trapping.getBasePrice();
		
		//Assert
		final double EXPECTED_PRICE = trapping.getUnitPrice();
		assertEquals(EXPECTED_PRICE, basePrice, 0.0);
		
	}
	
	@Test
	public void getPrice_shouldReturnPriceForTrappingConsideringNbPersons(){
		//Act
		double price = this.trapping.getPrice();
		
		//Assert
		final double EXPECTED_PRICE = trapping.getUnitPrice() * ANY_NB_PERSONS;
		assertEquals(EXPECTED_PRICE, price, 0.0);
	}
	
	@Test
	public void getTotalPrice_shouldReturnPriceForTrappingConsideringNbDaysAndOtherDecorations(){
		//Arrange
		Trapping trapping2 = new Trapping(this.trapping, ANY_NB_PERSONS);
		
		//Act
		double totalPrice = trapping2.getTotalPrice();
		
		//Assert
		final double EXPECTED_PRICE = (trapping.getUnitPrice() * ANY_NB_PERSONS) * 2;
		assertEquals(EXPECTED_PRICE, totalPrice, 0.0);
	}
	
	@Test (expected = InvalidPersonNumberException.class)
	public void createTrappingObservation_shouldThrowAnExceptionIfNbPersonBelowMinimum(){
		
		//Act
		Trapping trapping = new Trapping(this.cottage, Trapping.MINIMUM_NB_PERSON -1);
	}
}
