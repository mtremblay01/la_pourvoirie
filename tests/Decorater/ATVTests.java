package Decorater;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import Mocks.CottageMock;

public class ATVTests {

	public static final int ANY_NB_DAYS = 5;
	private ATV atv;
	private CottageMock cottage;
	
	@Before
	public void createCleaning(){
		this.cottage = new CottageMock();
		this.atv = new ATV(this.cottage, ANY_NB_DAYS);
	}

	@Test
	public void getUnitPrice_ShouldReturnUnitPriceForTheATVService(){
		//Act
		double unitPrice = atv.getUnitPrice();
		
		//Assert
		final double EXPECTED_UNIT_PRICE = atv.getUnitPrice();
		assertEquals(EXPECTED_UNIT_PRICE, unitPrice, 0.00);
	}
	
	@Test
	public void setUnitPrice_shouldChangeUnitPriceForTheATVService(){
		//Arrange
		final double ANY_UNIT_PRICE = 10;
		
		//Act
		atv.setUnitPrice(ANY_UNIT_PRICE);
		
		//Assert
		double unitPrice = atv.getUnitPrice();
		assertEquals(ANY_UNIT_PRICE, unitPrice, 0.0);
	}
	
	@Test
	public void getBasePrice_shouldReturnBasePriceForATVService(){
		//Act
		double basePrice = this.atv.getBasePrice();
		
		//Assert
		final double EXPECTED_PRICE = atv.DEFAULT_UNIT_PRICE;
		assertEquals(EXPECTED_PRICE, basePrice, 0.0);
		
	}
	
	@Test
	public void getPrice_shouldReturnPriceForATVServiceConsideringNbDays(){
		//Act
		double price = this.atv.getPrice();
		
		//Assert
		final double EXPECTED_PRICE = atv.getUnitPrice() * ANY_NB_DAYS;
		assertEquals(EXPECTED_PRICE, price, 0.0);
	}
	
	@Test
	public void getTotalPrice_shouldReturnPriceForATVServiceConsideringNbDaysAndOtherDecorations(){
		//Arrange
		ATV atv2 = new ATV(this.atv, ANY_NB_DAYS);
		
		//Act
		double totalPrice = atv2.getTotalPrice();
		
		//Assert
		final double EXPECTED_PRICE = (atv.getUnitPrice() * ANY_NB_DAYS) * 2;
		assertEquals(EXPECTED_PRICE, totalPrice, 0.0);
	}

}
