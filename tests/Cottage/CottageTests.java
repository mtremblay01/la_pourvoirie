package Cottage;
import static org.junit.Assert.*;

import org.junit.Test;

import Cottage.CottageLarge;
import Cottage.CottageSmall;
import Cottage.CottageMedium;

public class CottageTests {

	@Test
	public void GIVEN_Cottage4person_WHEN_TotalPriceIsAsked_THEN_PriceIsReturned() {
		int days = 2;
		double totalPrice = CottageSmall.UNIT_PRICE*days;
		CottageSmall cottage = new CottageSmall(days);
		
		assertEquals(cottage.getTotalPrice(), totalPrice, 0.0);
	}

	@Test
	public void GIVEN_Cottage6person_WHEN_TotalPriceIsAsked_THEN_PriceIsReturned() {
		int days = 2;
		double totalPrice = CottageMedium.UNIT_PRICE*days;
		CottageMedium cottage = new CottageMedium(days);
		
		assertEquals(cottage.getTotalPrice(), totalPrice, 0.0);
	}
	@Test
	public void GIVEN_Cottage10person_WHEN_TotalPriceIsAsked_THEN_PriceIsReturned() {
		int days = 2;
		double totalPrice = CottageLarge.UNIT_PRICE*days;
		CottageLarge cottage = new CottageLarge(days);
		
		assertEquals(cottage.getTotalPrice(), totalPrice, 0.0);
	}
	
	@Test
	public void GIVEN_Cottage4person_WHEN_PriceIsAsked_THEN_PriceIsReturned() {
		int days = 2;
		double totalPrice = CottageSmall.UNIT_PRICE*days;
		CottageSmall cottage = new CottageSmall(days);
		
		assertEquals(cottage.getPrice(), totalPrice, 0.0);
	}

	@Test
	public void GIVEN_Cottage6person_WHEN_PriceIsAsked_THEN_PriceIsReturned() {
		int days = 2;
		double totalPrice = CottageMedium.UNIT_PRICE*days;
		CottageMedium cottage = new CottageMedium(days);
		
		assertEquals(cottage.getPrice(), totalPrice, 0.0);
	}
	@Test
	public void GIVEN_Cottage10person_WHEN_PriceIsAsked_THEN_PriceIsReturned() {
		int days = 2;
		double totalPrice = CottageLarge.UNIT_PRICE*days;
		CottageLarge cottage = new CottageLarge(days);
		
		assertEquals(cottage.getPrice(), totalPrice, 0.0);
	}
	
	
	@Test
	public void GIVEN_Cottage4person_WHEN_BasePriceIsAsked_THEN_PriceIsReturned() {
		int days = 2;
		double totalPrice = CottageSmall.UNIT_PRICE;
		CottageSmall cottage = new CottageSmall(days);
		
		assertEquals(cottage.getBasePrice(), totalPrice, 0.0);
	}

	@Test
	public void GIVEN_Cottage6person_WHEN_BasePriceIsAsked_THEN_PriceIsReturned() {
		int days = 2;
		double totalPrice = CottageMedium.UNIT_PRICE;
		CottageMedium cottage = new CottageMedium(days);
		
		assertEquals(cottage.getBasePrice(), totalPrice, 0.0);
	}
	@Test
	public void GIVEN_Cottage10person_WHEN_BasePriceIsAsked_THEN_PriceIsReturned() {
		int days = 2;
		double totalPrice = CottageLarge.UNIT_PRICE;
		CottageLarge cottage = new CottageLarge(days);
		
		assertEquals(cottage.getBasePrice(), totalPrice, 0.0);
	}
}
