package CottageBuilder;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import Cottage.CottageBase;
import Cottage.CottageLarge;
import Cottage.CottageMedium;
import Cottage.CottageSmall;
import Decorater.ATV;
import Decorater.BearObservation;
import Decorater.BeaverObservation;
import Decorater.Boat;
import Decorater.BreakfastAndLunch;
import Decorater.Cleaning;
import Decorater.Dinner;
import Decorater.Fishing;
import Decorater.FlyCrafting;
import Decorater.FlyFishing;
import Decorater.GastronomicDinner;
import Decorater.Hunting;
import Decorater.HydroAirplane;
import Decorater.Trapping;
import Decorater.WolfObservation;
import exceptions.AlreadySelectedDinnerException;
import exceptions.InvalidNumberDaysException;
import exceptions.InvalidNumberOfTransportsException;
import exceptions.InvalidPersonNumberException;
import exceptions.NoDinnerException;

public class CottageBuilderTests {

	//chaque methode
	//chaque exception
	//cout total
	//done
	
	public static final int ANY_NB_DAYS = 5;
	public static final int ANY_NB_PERSONS = 4;
	
	private CottageBuilder cottageBuilder;
	
	
	@Before
	public void createBuilder(){
		this.cottageBuilder = new CottageBuilder();
	}
	
	@Test
	public void buildCottage_shouldCreateBasicCottageWithSmallCottageSize() {
		//Arrange		
		//this.cottageBuilder = this.cottageBuilder.buildCottage(CottageBuilder.CottageSize.SMALL, ANY_NB_DAYS, ANY_NB_PERSONS).
		//																					withBoat().withBoat().withDinner();
		this.cottageBuilder = this.cottageBuilder.buildCottage(CottageBuilder.CottageSize.SMALL, ANY_NB_DAYS, ANY_NB_PERSONS);
		this.cottageBuilder = this.cottageBuilder.withBoat();
		this.cottageBuilder = this.cottageBuilder.withBoat();
		this.cottageBuilder = this.cottageBuilder.withDinner();
		
		CottageBase cottage = this.cottageBuilder.done();
		
		//Act
		double actualPrice = cottage.getTotalPrice();
				
		//Assert
		final double EXPECTED_PRICE = (ANY_NB_DAYS * CottageSmall.UNIT_PRICE) + (Boat.DEFAULT_UNIT_PRICE * ANY_NB_PERSONS) * 2 + (Dinner.DEFAULT_UNIT_PRICE * ANY_NB_DAYS * ANY_NB_PERSONS);
		assertEquals(EXPECTED_PRICE, actualPrice, 0.00);
	}
	
	@Test
	public void buildCottage_shouldCreateBasicCottageWithMediumCottageSize() {
		//Arrange		
		//this.cottageBuilder = this.cottageBuilder.buildCottage(CottageBuilder.CottageSize.SMALL, ANY_NB_DAYS, ANY_NB_PERSONS).
		//																					withBoat().withBoat().withDinner();
		this.cottageBuilder = this.cottageBuilder.buildCottage(CottageBuilder.CottageSize.MEDIUM, ANY_NB_DAYS, ANY_NB_PERSONS);
		this.cottageBuilder = this.cottageBuilder.withBoat();
		this.cottageBuilder = this.cottageBuilder.withBoat();
		this.cottageBuilder = this.cottageBuilder.withDinner();
		
		CottageBase cottage = this.cottageBuilder.done();
		
		//Act
		double actualPrice = cottage.getTotalPrice();
				
		//Assert
		final double EXPECTED_PRICE = (ANY_NB_DAYS * CottageMedium.UNIT_PRICE) + (Boat.DEFAULT_UNIT_PRICE * ANY_NB_PERSONS) * 2 + (Dinner.DEFAULT_UNIT_PRICE * ANY_NB_DAYS * ANY_NB_PERSONS);
		assertEquals(EXPECTED_PRICE, actualPrice, 0.00);
	}
	
	@Test
	public void buildCottage_shouldCreateBasicCottageWithLargeCottageSize() {
		//Arrange		
		//this.cottageBuilder = this.cottageBuilder.buildCottage(CottageBuilder.CottageSize.SMALL, ANY_NB_DAYS, ANY_NB_PERSONS).
		//																					withBoat().withBoat().withDinner();
		this.cottageBuilder = this.cottageBuilder.buildCottage(CottageBuilder.CottageSize.LARGE, ANY_NB_DAYS, ANY_NB_PERSONS);
		this.cottageBuilder = this.cottageBuilder.withBoat();
		this.cottageBuilder = this.cottageBuilder.withBoat();
		this.cottageBuilder = this.cottageBuilder.withDinner();
		
		CottageBase cottage = this.cottageBuilder.done();
		
		//Act
		double actualPrice = cottage.getTotalPrice();
				
		//Assert
		final double EXPECTED_PRICE = (ANY_NB_DAYS * CottageLarge.UNIT_PRICE) + (Boat.DEFAULT_UNIT_PRICE * ANY_NB_PERSONS) * 2 + (Dinner.DEFAULT_UNIT_PRICE * ANY_NB_DAYS * ANY_NB_PERSONS);
		assertEquals(EXPECTED_PRICE, actualPrice, 0.00);
	}
	
	@Test
	public void buildCottage_shouldCreateCottageWithSmallCottageSizeAndHydroplaneAsSecondaryTransport() {
		//Arrange		
		//this.cottageBuilder = this.cottageBuilder.buildCottage(CottageBuilder.CottageSize.SMALL, ANY_NB_DAYS, ANY_NB_PERSONS).
		//																					withBoat().withBoat().withDinner();
		this.cottageBuilder = this.cottageBuilder.buildCottage(CottageBuilder.CottageSize.SMALL, ANY_NB_DAYS, ANY_NB_PERSONS);
		this.cottageBuilder = this.cottageBuilder.withBoat();
		this.cottageBuilder = this.cottageBuilder.withHydroAirplane();
		this.cottageBuilder = this.cottageBuilder.withDinner();
		
		CottageBase cottage = this.cottageBuilder.done();
		
		//Act
		double actualPrice = cottage.getTotalPrice();
				
		//Assert
		final double EXPECTED_PRICE = (ANY_NB_DAYS * CottageSmall.UNIT_PRICE) + Boat.DEFAULT_UNIT_PRICE * ANY_NB_PERSONS
										+ HydroAirplane.DEFAULT_UNIT_PRICE * ANY_NB_PERSONS + (Dinner.DEFAULT_UNIT_PRICE * ANY_NB_DAYS * ANY_NB_PERSONS);
		assertEquals(EXPECTED_PRICE, actualPrice, 0.00);
	}
	
	@Test
	public void buildCottage_shouldCreateCottageWithSmallCottageSizeAndATV() {
		//Arrange		
		//this.cottageBuilder = this.cottageBuilder.buildCottage(CottageBuilder.CottageSize.SMALL, ANY_NB_DAYS, ANY_NB_PERSONS).
		//																					withBoat().withBoat().withDinner();
		this.cottageBuilder = this.cottageBuilder.buildCottage(CottageBuilder.CottageSize.SMALL, ANY_NB_DAYS, ANY_NB_PERSONS);
		this.cottageBuilder = this.cottageBuilder.withBoat();
		this.cottageBuilder = this.cottageBuilder.withHydroAirplane();
		this.cottageBuilder = this.cottageBuilder.withDinner();
		this.cottageBuilder = this.cottageBuilder.withATV(ANY_NB_DAYS);
		
		CottageBase cottage = this.cottageBuilder.done();
		
		//Act
		double actualPrice = cottage.getTotalPrice();
				
		//Assert
		final double EXPECTED_PRICE = (ANY_NB_DAYS * CottageSmall.UNIT_PRICE) + (Boat.DEFAULT_UNIT_PRICE * ANY_NB_PERSONS)
										+ (HydroAirplane.DEFAULT_UNIT_PRICE * ANY_NB_PERSONS) + (Dinner.DEFAULT_UNIT_PRICE * ANY_NB_DAYS * ANY_NB_PERSONS)
										 + ATV.DEFAULT_UNIT_PRICE * ANY_NB_DAYS;
		assertEquals(EXPECTED_PRICE, actualPrice, 0.00);
	}
	
	@Test
	public void buildCottage_shouldCreateCottageWithSmallCottageSizeAndBearObservation() {
		//Arrange		
		//this.cottageBuilder = this.cottageBuilder.buildCottage(CottageBuilder.CottageSize.SMALL, ANY_NB_DAYS, ANY_NB_PERSONS).
		//																					withBoat().withBoat().withDinner();
		this.cottageBuilder = this.cottageBuilder.buildCottage(CottageBuilder.CottageSize.SMALL, ANY_NB_DAYS, ANY_NB_PERSONS);
		this.cottageBuilder = this.cottageBuilder.withBoat();
		this.cottageBuilder = this.cottageBuilder.withHydroAirplane();
		this.cottageBuilder = this.cottageBuilder.withDinner();
		this.cottageBuilder = this.cottageBuilder.withBearObservation(ANY_NB_PERSONS);
		
		CottageBase cottage = this.cottageBuilder.done();
		
		//Act
		double actualPrice = cottage.getTotalPrice();
				
		//Assert
		final double EXPECTED_PRICE = (ANY_NB_DAYS * CottageSmall.UNIT_PRICE) + (Boat.DEFAULT_UNIT_PRICE * ANY_NB_PERSONS)
										+ (HydroAirplane.DEFAULT_UNIT_PRICE * ANY_NB_PERSONS) + (Dinner.DEFAULT_UNIT_PRICE * ANY_NB_DAYS * ANY_NB_PERSONS)
										 + BearObservation.DEFAULT_UNIT_PRICE * ANY_NB_PERSONS;
		assertEquals(EXPECTED_PRICE, actualPrice, 0.00);
	}
	
	@Test
	public void buildCottage_shouldCreateCottageWithSmallCottageSizeAndBeaverObservation() {
		//Arrange		
		//this.cottageBuilder = this.cottageBuilder.buildCottage(CottageBuilder.CottageSize.SMALL, ANY_NB_DAYS, ANY_NB_PERSONS).
		//																					withBoat().withBoat().withDinner();
		this.cottageBuilder = this.cottageBuilder.buildCottage(CottageBuilder.CottageSize.SMALL, ANY_NB_DAYS, ANY_NB_PERSONS);
		this.cottageBuilder = this.cottageBuilder.withBoat();
		this.cottageBuilder = this.cottageBuilder.withHydroAirplane();
		this.cottageBuilder = this.cottageBuilder.withDinner();
		this.cottageBuilder = this.cottageBuilder.withBeaverObservation(ANY_NB_PERSONS);
		
		CottageBase cottage = this.cottageBuilder.done();
		
		//Act
		double actualPrice = cottage.getTotalPrice();
				
		//Assert
		final double EXPECTED_PRICE = (ANY_NB_DAYS * CottageSmall.UNIT_PRICE) + (Boat.DEFAULT_UNIT_PRICE * ANY_NB_PERSONS)
										+ (HydroAirplane.DEFAULT_UNIT_PRICE * ANY_NB_PERSONS) + (Dinner.DEFAULT_UNIT_PRICE * ANY_NB_DAYS * ANY_NB_PERSONS)
										 + BeaverObservation.DEFAULT_UNIT_PRICE * ANY_NB_PERSONS;
		assertEquals(EXPECTED_PRICE, actualPrice, 0.00);
	}
	
	@Test
	public void buildCottage_shouldCreateCottageWithSmallCottageSizeAndBreakfastAndLunch() {
		//Arrange		
		//this.cottageBuilder = this.cottageBuilder.buildCottage(CottageBuilder.CottageSize.SMALL, ANY_NB_DAYS, ANY_NB_PERSONS).
		//																					withBoat().withBoat().withDinner();
		this.cottageBuilder = this.cottageBuilder.buildCottage(CottageBuilder.CottageSize.SMALL, ANY_NB_DAYS, ANY_NB_PERSONS);
		this.cottageBuilder = this.cottageBuilder.withBoat();
		this.cottageBuilder = this.cottageBuilder.withHydroAirplane();
		this.cottageBuilder = this.cottageBuilder.withDinner();
		this.cottageBuilder = this.cottageBuilder.withBreakfastAndLunch(ANY_NB_PERSONS, ANY_NB_DAYS);
		
		CottageBase cottage = this.cottageBuilder.done();
		
		//Act
		double actualPrice = cottage.getTotalPrice();
				
		//Assert
		final double EXPECTED_PRICE = (ANY_NB_DAYS * CottageSmall.UNIT_PRICE) + (Boat.DEFAULT_UNIT_PRICE * ANY_NB_PERSONS)
										+ (HydroAirplane.DEFAULT_UNIT_PRICE * ANY_NB_PERSONS) + (Dinner.DEFAULT_UNIT_PRICE * ANY_NB_DAYS * ANY_NB_PERSONS)
										 + BreakfastAndLunch.DEFAULT_UNIT_PRICE * ANY_NB_PERSONS * ANY_NB_DAYS;
		assertEquals(EXPECTED_PRICE, actualPrice, 0.00);
	}
	
	@Test
	public void buildCottage_shouldCreateCottageWithSmallCottageSizeAndCleaningService() {
		//Arrange		
		//this.cottageBuilder = this.cottageBuilder.buildCottage(CottageBuilder.CottageSize.SMALL, ANY_NB_DAYS, ANY_NB_PERSONS).
		//																					withBoat().withBoat().withDinner();
		this.cottageBuilder = this.cottageBuilder.buildCottage(CottageBuilder.CottageSize.SMALL, ANY_NB_DAYS, ANY_NB_PERSONS);
		this.cottageBuilder = this.cottageBuilder.withBoat();
		this.cottageBuilder = this.cottageBuilder.withHydroAirplane();
		this.cottageBuilder = this.cottageBuilder.withDinner();
		this.cottageBuilder = this.cottageBuilder.withCleaningService(ANY_NB_DAYS);
		
		CottageBase cottage = this.cottageBuilder.done();
		
		//Act
		double actualPrice = cottage.getTotalPrice();
				
		//Assert
		final double EXPECTED_PRICE = (ANY_NB_DAYS * CottageSmall.UNIT_PRICE) + (Boat.DEFAULT_UNIT_PRICE * ANY_NB_PERSONS)
										+ (HydroAirplane.DEFAULT_UNIT_PRICE * ANY_NB_PERSONS) + (Dinner.DEFAULT_UNIT_PRICE * ANY_NB_DAYS * ANY_NB_PERSONS)
										 + Cleaning.DEFAULT_UNIT_PRICE * ANY_NB_DAYS;
		assertEquals(EXPECTED_PRICE, actualPrice, 0.00);
	}
	
	@Test
	public void buildCottage_shouldCreateCottageWithSmallCottageSizeAndFishing() {
		//Arrange		
		//this.cottageBuilder = this.cottageBuilder.buildCottage(CottageBuilder.CottageSize.SMALL, ANY_NB_DAYS, ANY_NB_PERSONS).
		//																					withBoat().withBoat().withDinner();
		this.cottageBuilder = this.cottageBuilder.buildCottage(CottageBuilder.CottageSize.SMALL, ANY_NB_DAYS, ANY_NB_PERSONS);
		this.cottageBuilder = this.cottageBuilder.withBoat();
		this.cottageBuilder = this.cottageBuilder.withHydroAirplane();
		this.cottageBuilder = this.cottageBuilder.withDinner();
		this.cottageBuilder = this.cottageBuilder.withFishing(ANY_NB_PERSONS);
		
		CottageBase cottage = this.cottageBuilder.done();
		
		//Act
		double actualPrice = cottage.getTotalPrice();
				
		//Assert
		final double EXPECTED_PRICE = (ANY_NB_DAYS * CottageSmall.UNIT_PRICE) + (Boat.DEFAULT_UNIT_PRICE * ANY_NB_PERSONS)
										+ (HydroAirplane.DEFAULT_UNIT_PRICE * ANY_NB_PERSONS) + (Dinner.DEFAULT_UNIT_PRICE * ANY_NB_DAYS * ANY_NB_PERSONS)
										 + Fishing.DEFAULT_UNIT_PRICE * ANY_NB_PERSONS;
		assertEquals(EXPECTED_PRICE, actualPrice, 0.00);
	}
	
	@Test
	public void buildCottage_shouldCreateCottageWithSmallCottageSizeAndFlyCrafting() {
		//Arrange		
		//this.cottageBuilder = this.cottageBuilder.buildCottage(CottageBuilder.CottageSize.SMALL, ANY_NB_DAYS, ANY_NB_PERSONS).
		//																					withBoat().withBoat().withDinner();
		this.cottageBuilder = this.cottageBuilder.buildCottage(CottageBuilder.CottageSize.SMALL, ANY_NB_DAYS, ANY_NB_PERSONS);
		this.cottageBuilder = this.cottageBuilder.withBoat();
		this.cottageBuilder = this.cottageBuilder.withHydroAirplane();
		this.cottageBuilder = this.cottageBuilder.withDinner();
		this.cottageBuilder = this.cottageBuilder.withFlyCrafting(ANY_NB_PERSONS);
		
		CottageBase cottage = this.cottageBuilder.done();
		
		//Act
		double actualPrice = cottage.getTotalPrice();
				
		//Assert
		final double EXPECTED_PRICE = (ANY_NB_DAYS * CottageSmall.UNIT_PRICE) + (Boat.DEFAULT_UNIT_PRICE * ANY_NB_PERSONS)
										+ (HydroAirplane.DEFAULT_UNIT_PRICE * ANY_NB_PERSONS) + (Dinner.DEFAULT_UNIT_PRICE * ANY_NB_DAYS * ANY_NB_PERSONS)
										 + FlyCrafting.DEFAULT_UNIT_PRICE * ANY_NB_PERSONS;
		assertEquals(EXPECTED_PRICE, actualPrice, 0.00);
	}
	
	@Test
	public void buildCottage_shouldCreateCottageWithSmallCottageSizeAndFlyFishing() {
		//Arrange		
		//this.cottageBuilder = this.cottageBuilder.buildCottage(CottageBuilder.CottageSize.SMALL, ANY_NB_DAYS, ANY_NB_PERSONS).
		//																					withBoat().withBoat().withDinner();
		this.cottageBuilder = this.cottageBuilder.buildCottage(CottageBuilder.CottageSize.SMALL, ANY_NB_DAYS, ANY_NB_PERSONS);
		this.cottageBuilder = this.cottageBuilder.withBoat();
		this.cottageBuilder = this.cottageBuilder.withHydroAirplane();
		this.cottageBuilder = this.cottageBuilder.withDinner();
		this.cottageBuilder = this.cottageBuilder.withFlyFishing(ANY_NB_PERSONS);
		
		CottageBase cottage = this.cottageBuilder.done();
		
		//Act
		double actualPrice = cottage.getTotalPrice();
				
		//Assert
		final double EXPECTED_PRICE = (ANY_NB_DAYS * CottageSmall.UNIT_PRICE) + (Boat.DEFAULT_UNIT_PRICE * ANY_NB_PERSONS)
										+ (HydroAirplane.DEFAULT_UNIT_PRICE * ANY_NB_PERSONS) + (Dinner.DEFAULT_UNIT_PRICE * ANY_NB_DAYS * ANY_NB_PERSONS)
										 + FlyFishing.DEFAULT_UNIT_PRICE * ANY_NB_PERSONS;
		assertEquals(EXPECTED_PRICE, actualPrice, 0.00);
	}
	
	@Test
	public void buildCottage_shouldCreateCottageWithSmallCottageSizeAndGastronomicDinner() {
		//Arrange		
		//this.cottageBuilder = this.cottageBuilder.buildCottage(CottageBuilder.CottageSize.SMALL, ANY_NB_DAYS, ANY_NB_PERSONS).
		//																					withBoat().withBoat().withDinner();
		this.cottageBuilder = this.cottageBuilder.buildCottage(CottageBuilder.CottageSize.SMALL, ANY_NB_DAYS, ANY_NB_PERSONS);
		this.cottageBuilder = this.cottageBuilder.withBoat();
		this.cottageBuilder = this.cottageBuilder.withHydroAirplane();
		this.cottageBuilder = this.cottageBuilder.withDinner();
		this.cottageBuilder = this.cottageBuilder.withGastronomicDinner(ANY_NB_PERSONS, ANY_NB_DAYS);
		
		CottageBase cottage = this.cottageBuilder.done();
		
		//Act
		double actualPrice = cottage.getTotalPrice();
				
		//Assert
		final double EXPECTED_PRICE = (ANY_NB_DAYS * CottageSmall.UNIT_PRICE) + (Boat.DEFAULT_UNIT_PRICE * ANY_NB_PERSONS)
										+ (HydroAirplane.DEFAULT_UNIT_PRICE * ANY_NB_PERSONS) + (Dinner.DEFAULT_UNIT_PRICE * ANY_NB_DAYS * ANY_NB_PERSONS)
										 + GastronomicDinner.DEFAULT_UNIT_PRICE * ANY_NB_PERSONS * ANY_NB_DAYS;
		assertEquals(EXPECTED_PRICE, actualPrice, 0.00);
	}
	
	@Test
	public void buildCottage_shouldCreateCottageWithSmallCottageSizeAndHunting() {
		//Arrange		
		//this.cottageBuilder = this.cottageBuilder.buildCottage(CottageBuilder.CottageSize.SMALL, ANY_NB_DAYS, ANY_NB_PERSONS).
		//																					withBoat().withBoat().withDinner();
		this.cottageBuilder = this.cottageBuilder.buildCottage(CottageBuilder.CottageSize.SMALL, ANY_NB_DAYS, ANY_NB_PERSONS);
		this.cottageBuilder = this.cottageBuilder.withBoat();
		this.cottageBuilder = this.cottageBuilder.withHydroAirplane();
		this.cottageBuilder = this.cottageBuilder.withDinner();
		this.cottageBuilder = this.cottageBuilder.withHunting(ANY_NB_PERSONS);
		
		CottageBase cottage = this.cottageBuilder.done();
		
		//Act
		double actualPrice = cottage.getTotalPrice();
				
		//Assert
		final double EXPECTED_PRICE = (ANY_NB_DAYS * CottageSmall.UNIT_PRICE) + (Boat.DEFAULT_UNIT_PRICE * ANY_NB_PERSONS)
										+ (HydroAirplane.DEFAULT_UNIT_PRICE * ANY_NB_PERSONS) + (Dinner.DEFAULT_UNIT_PRICE * ANY_NB_DAYS * ANY_NB_PERSONS)
										 + Hunting.DEFAULT_UNIT_PRICE * ANY_NB_PERSONS;
		assertEquals(EXPECTED_PRICE, actualPrice, 0.00);
	}
	
	@Test
	public void buildCottage_shouldCreateCottageWithSmallCottageSizeAndTrapping() {
		//Arrange		
		//this.cottageBuilder = this.cottageBuilder.buildCottage(CottageBuilder.CottageSize.SMALL, ANY_NB_DAYS, ANY_NB_PERSONS).
		//																					withBoat().withBoat().withDinner();
		this.cottageBuilder = this.cottageBuilder.buildCottage(CottageBuilder.CottageSize.SMALL, ANY_NB_DAYS, ANY_NB_PERSONS);
		this.cottageBuilder = this.cottageBuilder.withBoat();
		this.cottageBuilder = this.cottageBuilder.withHydroAirplane();
		this.cottageBuilder = this.cottageBuilder.withDinner();
		this.cottageBuilder = this.cottageBuilder.withTrapping(ANY_NB_PERSONS);
		
		CottageBase cottage = this.cottageBuilder.done();
		
		//Act
		double actualPrice = cottage.getTotalPrice();
				
		//Assert
		final double EXPECTED_PRICE = (ANY_NB_DAYS * CottageSmall.UNIT_PRICE) + (Boat.DEFAULT_UNIT_PRICE * ANY_NB_PERSONS)
										+ (HydroAirplane.DEFAULT_UNIT_PRICE * ANY_NB_PERSONS) + (Dinner.DEFAULT_UNIT_PRICE * ANY_NB_DAYS * ANY_NB_PERSONS)
										 + Trapping.DEFAULT_UNIT_PRICE * ANY_NB_PERSONS;
		assertEquals(EXPECTED_PRICE, actualPrice, 0.00);
	}
	
	@Test
	public void buildCottage_shouldCreateCottageWithSmallCottageSizeAndWolfObservation() {
		//Arrange		
		//this.cottageBuilder = this.cottageBuilder.buildCottage(CottageBuilder.CottageSize.SMALL, ANY_NB_DAYS, ANY_NB_PERSONS).
		//																					withBoat().withBoat().withDinner();
		this.cottageBuilder = this.cottageBuilder.buildCottage(CottageBuilder.CottageSize.SMALL, ANY_NB_DAYS, ANY_NB_PERSONS);
		this.cottageBuilder = this.cottageBuilder.withBoat();
		this.cottageBuilder = this.cottageBuilder.withHydroAirplane();
		this.cottageBuilder = this.cottageBuilder.withDinner();
		this.cottageBuilder = this.cottageBuilder.withWolfObservation(ANY_NB_PERSONS);
		
		CottageBase cottage = this.cottageBuilder.done();
		
		//Act
		double actualPrice = cottage.getTotalPrice();
				
		//Assert
		final double EXPECTED_PRICE = (ANY_NB_DAYS * CottageSmall.UNIT_PRICE) + (Boat.DEFAULT_UNIT_PRICE * ANY_NB_PERSONS)
										+ (HydroAirplane.DEFAULT_UNIT_PRICE * ANY_NB_PERSONS) + (Dinner.DEFAULT_UNIT_PRICE * ANY_NB_DAYS * ANY_NB_PERSONS)
										 + WolfObservation.DEFAULT_UNIT_PRICE * ANY_NB_PERSONS;
		assertEquals(EXPECTED_PRICE, actualPrice, 0.00);
	}
	
	@Test(expected = InvalidNumberOfTransportsException.class)
	public void buildCottage_shouldThrowAnExceptionWhenNumberOfTranportNotValid(){
		//Arrange
		this.cottageBuilder = this.cottageBuilder.buildCottage(CottageBuilder.CottageSize.SMALL, ANY_NB_DAYS, ANY_NB_PERSONS);
		this.cottageBuilder = this.cottageBuilder.withBoat();
		this.cottageBuilder = this.cottageBuilder.withDinner();
		
		//Act
		
		this.cottageBuilder.done();
	}
	
	@Test(expected = NoDinnerException.class)
	public void buildCottage_shouldThrowAnExceptionWhenNoDinner(){
		//Arrange
		this.cottageBuilder = this.cottageBuilder.buildCottage(CottageBuilder.CottageSize.SMALL, ANY_NB_DAYS, ANY_NB_PERSONS);
		this.cottageBuilder = this.cottageBuilder.withBoat();
		this.cottageBuilder = this.cottageBuilder.withBoat();
		
		//Act
		
		this.cottageBuilder.done();
	}
	
	@Test(expected = InvalidNumberDaysException.class)
	public void buildCottage_shouldThrowAnExceptionWhenNumberOfDaysForATVNotValid(){
		//Arrange
		this.cottageBuilder = this.cottageBuilder.buildCottage(CottageBuilder.CottageSize.SMALL, ANY_NB_DAYS, ANY_NB_PERSONS);
		this.cottageBuilder = this.cottageBuilder.withBoat();
		this.cottageBuilder = this.cottageBuilder.withBoat();
		this.cottageBuilder = this.cottageBuilder.withDinner();
		this.cottageBuilder = this.cottageBuilder.withATV(ANY_NB_DAYS + 1);
		
		//Act
		
		this.cottageBuilder.done();
	}
	
	@Test(expected = InvalidPersonNumberException.class)
	public void buildCottage_shouldThrowAnExceptionWhenNumberOfPeopleForBearObservationNotValid(){
		//Arrange
		this.cottageBuilder = this.cottageBuilder.buildCottage(CottageBuilder.CottageSize.SMALL, ANY_NB_DAYS, ANY_NB_PERSONS);
		this.cottageBuilder = this.cottageBuilder.withBoat();
		this.cottageBuilder = this.cottageBuilder.withBoat();
		this.cottageBuilder = this.cottageBuilder.withDinner();
		this.cottageBuilder = this.cottageBuilder.withBearObservation(ANY_NB_PERSONS + 1);
		
		//Act
		
		this.cottageBuilder.done();
	}
	
	@Test(expected = InvalidPersonNumberException.class)
	public void buildCottage_shouldThrowAnExceptionWhenNumberOfPeopleForBeaverObservationNotValid(){
		//Arrange
		this.cottageBuilder = this.cottageBuilder.buildCottage(CottageBuilder.CottageSize.SMALL, ANY_NB_DAYS, ANY_NB_PERSONS);
		this.cottageBuilder = this.cottageBuilder.withBoat();
		this.cottageBuilder = this.cottageBuilder.withBoat();
		this.cottageBuilder = this.cottageBuilder.withDinner();
		this.cottageBuilder = this.cottageBuilder.withBeaverObservation(ANY_NB_PERSONS + 1);
		
		//Act
		
		this.cottageBuilder.done();
	}
	
	@Test(expected = InvalidPersonNumberException.class)
	public void buildCottage_shouldThrowAnExceptionWhenNumberOfPeopleForWolfObservationNotValid(){
		//Arrange
		this.cottageBuilder = this.cottageBuilder.buildCottage(CottageBuilder.CottageSize.SMALL, ANY_NB_DAYS, ANY_NB_PERSONS);
		this.cottageBuilder = this.cottageBuilder.withBoat();
		this.cottageBuilder = this.cottageBuilder.withBoat();
		this.cottageBuilder = this.cottageBuilder.withDinner();
		this.cottageBuilder = this.cottageBuilder.withWolfObservation(ANY_NB_PERSONS + 1);
		
		//Act
		
		this.cottageBuilder.done();
	}
	
	@Test(expected = InvalidPersonNumberException.class)
	public void buildCottage_shouldThrowAnExceptionWhenNumberOfPeopleForBreakfastAndLunchNotValid(){
		//Arrange
		this.cottageBuilder = this.cottageBuilder.buildCottage(CottageBuilder.CottageSize.SMALL, ANY_NB_DAYS, ANY_NB_PERSONS);
		this.cottageBuilder = this.cottageBuilder.withBoat();
		this.cottageBuilder = this.cottageBuilder.withBoat();
		this.cottageBuilder = this.cottageBuilder.withDinner();
		this.cottageBuilder = this.cottageBuilder.withBreakfastAndLunch(ANY_NB_PERSONS +1, ANY_NB_DAYS);
		
		//Act
		
		this.cottageBuilder.done();
	}
	
	@Test(expected = InvalidNumberDaysException.class)
	public void buildCottage_shouldThrowAnExceptionWhenNumberOfDaysForBreakfastAndLunchNotValid(){
		//Arrange
		this.cottageBuilder = this.cottageBuilder.buildCottage(CottageBuilder.CottageSize.SMALL, ANY_NB_DAYS, ANY_NB_PERSONS);
		this.cottageBuilder = this.cottageBuilder.withBoat();
		this.cottageBuilder = this.cottageBuilder.withBoat();
		this.cottageBuilder = this.cottageBuilder.withDinner();
		this.cottageBuilder = this.cottageBuilder.withBreakfastAndLunch(ANY_NB_PERSONS, ANY_NB_DAYS +1);
		
		//Act
		
		this.cottageBuilder.done();
	}
	
	@Test(expected = InvalidNumberDaysException.class)
	public void buildCottage_shouldThrowAnExceptionWhenNumberOfDaysForCleaningServiceNotValid(){
		//Arrange
		this.cottageBuilder = this.cottageBuilder.buildCottage(CottageBuilder.CottageSize.SMALL, ANY_NB_DAYS, ANY_NB_PERSONS);
		this.cottageBuilder = this.cottageBuilder.withBoat();
		this.cottageBuilder = this.cottageBuilder.withBoat();
		this.cottageBuilder = this.cottageBuilder.withDinner();
		this.cottageBuilder = this.cottageBuilder.withCleaningService(ANY_NB_DAYS +1);
		
		//Act
		
		this.cottageBuilder.done();
	}
	
	@Test(expected = InvalidPersonNumberException.class)
	public void buildCottage_shouldThrowAnExceptionWhenNumberOfPeopleForFishingNotValid(){
		//Arrange
		this.cottageBuilder = this.cottageBuilder.buildCottage(CottageBuilder.CottageSize.SMALL, ANY_NB_DAYS, ANY_NB_PERSONS);
		this.cottageBuilder = this.cottageBuilder.withBoat();
		this.cottageBuilder = this.cottageBuilder.withBoat();
		this.cottageBuilder = this.cottageBuilder.withDinner();
		this.cottageBuilder = this.cottageBuilder.withFishing(ANY_NB_PERSONS +1);
		
		//Act
		
		this.cottageBuilder.done();
	}
	
	@Test(expected = InvalidPersonNumberException.class)
	public void buildCottage_shouldThrowAnExceptionWhenNumberOfPeopleForHuntingNotValid(){
		//Arrange
		this.cottageBuilder = this.cottageBuilder.buildCottage(CottageBuilder.CottageSize.SMALL, ANY_NB_DAYS, ANY_NB_PERSONS);
		this.cottageBuilder = this.cottageBuilder.withBoat();
		this.cottageBuilder = this.cottageBuilder.withBoat();
		this.cottageBuilder = this.cottageBuilder.withDinner();
		this.cottageBuilder = this.cottageBuilder.withHunting(ANY_NB_PERSONS +1);
		
		//Act
		
		this.cottageBuilder.done();
	}
	
	@Test(expected = InvalidPersonNumberException.class)
	public void buildCottage_shouldThrowAnExceptionWhenNumberOfPeopleForFlyCraftingNotValid(){
		//Arrange
		this.cottageBuilder = this.cottageBuilder.buildCottage(CottageBuilder.CottageSize.SMALL, ANY_NB_DAYS, ANY_NB_PERSONS);
		this.cottageBuilder = this.cottageBuilder.withBoat();
		this.cottageBuilder = this.cottageBuilder.withBoat();
		this.cottageBuilder = this.cottageBuilder.withDinner();
		this.cottageBuilder = this.cottageBuilder.withFlyCrafting(ANY_NB_PERSONS + 1);
		
		//Act
		
		this.cottageBuilder.done();
	}
	
	@Test(expected = InvalidPersonNumberException.class)
	public void buildCottage_shouldThrowAnExceptionWhenNumberOfPeopleForFlyFishingNotValid(){
		//Arrange
		this.cottageBuilder = this.cottageBuilder.buildCottage(CottageBuilder.CottageSize.SMALL, ANY_NB_DAYS, ANY_NB_PERSONS);
		this.cottageBuilder = this.cottageBuilder.withBoat();
		this.cottageBuilder = this.cottageBuilder.withBoat();
		this.cottageBuilder = this.cottageBuilder.withDinner();
		this.cottageBuilder = this.cottageBuilder.withFlyFishing(ANY_NB_PERSONS + 1);
		
		//Act
		
		this.cottageBuilder.done();
	}
	
	@Test(expected = InvalidPersonNumberException.class)
	public void buildCottage_shouldThrowAnExceptionWhenNumberOfPeopleForGastronomicDinnerNotValid(){
		//Arrange
		this.cottageBuilder = this.cottageBuilder.buildCottage(CottageBuilder.CottageSize.SMALL, ANY_NB_DAYS, ANY_NB_PERSONS);
		this.cottageBuilder = this.cottageBuilder.withBoat();
		this.cottageBuilder = this.cottageBuilder.withBoat();
		this.cottageBuilder = this.cottageBuilder.withDinner();
		this.cottageBuilder = this.cottageBuilder.withGastronomicDinner(ANY_NB_PERSONS + 1, ANY_NB_DAYS);
		
		//Act
		
		this.cottageBuilder.done();
	}
	
	@Test(expected = InvalidNumberDaysException.class)
	public void buildCottage_shouldThrowAnExceptionWhenNumberOfDaysForGastronomicDinnerNotValid(){
		//Arrange
		this.cottageBuilder = this.cottageBuilder.buildCottage(CottageBuilder.CottageSize.SMALL, ANY_NB_DAYS, ANY_NB_PERSONS);
		this.cottageBuilder = this.cottageBuilder.withBoat();
		this.cottageBuilder = this.cottageBuilder.withBoat();
		this.cottageBuilder = this.cottageBuilder.withDinner();
		this.cottageBuilder = this.cottageBuilder.withGastronomicDinner(ANY_NB_PERSONS, ANY_NB_DAYS + 1);
		
		//Act
		
		this.cottageBuilder.done();
	}
	
	@Test(expected = InvalidPersonNumberException.class)
	public void buildCottage_shouldThrowAnExceptionWhenNumberOfPeopleForGastronomicTrappingNotValid(){
		//Arrange
		this.cottageBuilder = this.cottageBuilder.buildCottage(CottageBuilder.CottageSize.SMALL, ANY_NB_DAYS, ANY_NB_PERSONS);
		this.cottageBuilder = this.cottageBuilder.withBoat();
		this.cottageBuilder = this.cottageBuilder.withBoat();
		this.cottageBuilder = this.cottageBuilder.withDinner();
		this.cottageBuilder = this.cottageBuilder.withTrapping(ANY_NB_PERSONS + 1);
		
		//Act
		
		this.cottageBuilder.done();
	}
	
	@Test(expected = InvalidPersonNumberException.class)
	public void buildCottage_shouldThrowAnExceptionWhenSmallCottageIsCreatedWithTooManyPeople(){
		//Arrange
		this.cottageBuilder = this.cottageBuilder.buildCottage(CottageBuilder.CottageSize.SMALL, ANY_NB_DAYS, CottageSmall.MAXIMUM_PERSON + 1);
		this.cottageBuilder = this.cottageBuilder.withBoat();
		this.cottageBuilder = this.cottageBuilder.withBoat();
		this.cottageBuilder = this.cottageBuilder.withDinner();
		this.cottageBuilder = this.cottageBuilder.withTrapping(ANY_NB_PERSONS + 1);
		
		//Act
		
		this.cottageBuilder.done();
	}
	
	@Test(expected = InvalidPersonNumberException.class)
	public void buildCottage_shouldThrowAnExceptionWhenMediumCottageIsCreatedWithTooManyPeople(){
		//Arrange
		this.cottageBuilder = this.cottageBuilder.buildCottage(CottageBuilder.CottageSize.MEDIUM, ANY_NB_DAYS, CottageMedium.MAXIMUM_PERSON + 1);
		this.cottageBuilder = this.cottageBuilder.withBoat();
		this.cottageBuilder = this.cottageBuilder.withBoat();
		this.cottageBuilder = this.cottageBuilder.withDinner();
		this.cottageBuilder = this.cottageBuilder.withTrapping(ANY_NB_PERSONS + 1);
		
		//Act
		
		this.cottageBuilder.done();
	}
	
	@Test(expected = InvalidPersonNumberException.class)
	public void buildCottage_shouldThrowAnExceptionWhenLargeCottageIsCreatedWithTooManyPeople(){
		//Arrange
		this.cottageBuilder = this.cottageBuilder.buildCottage(CottageBuilder.CottageSize.LARGE, ANY_NB_DAYS, CottageLarge.MAXIMUM_PERSON + 1);
		this.cottageBuilder = this.cottageBuilder.withBoat();
		this.cottageBuilder = this.cottageBuilder.withBoat();
		this.cottageBuilder = this.cottageBuilder.withDinner();
		this.cottageBuilder = this.cottageBuilder.withTrapping(ANY_NB_PERSONS + 1);
		
		//Act
		
		this.cottageBuilder.done();
	}
	
	@Test(expected = AlreadySelectedDinnerException.class)
	public void buildCottage_shouldThrowAnExceptionWhenDinnerIsSelectedTwice(){
		//Arrange
		this.cottageBuilder = this.cottageBuilder.buildCottage(CottageBuilder.CottageSize.SMALL, ANY_NB_DAYS, ANY_NB_PERSONS);
		this.cottageBuilder = this.cottageBuilder.withBoat();
		this.cottageBuilder = this.cottageBuilder.withBoat();
		this.cottageBuilder = this.cottageBuilder.withDinner();
		this.cottageBuilder = this.cottageBuilder.withDinner();
		
		//Act
		
		this.cottageBuilder.done();
	}
}

