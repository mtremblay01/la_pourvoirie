package Decorater;

import Cottage.CottageBase;
import exceptions.InvalidPersonNumberException;

public class BearObservation extends IndividualServices {
	public static final double DEFAULT_UNIT_PRICE = 15;
	public static final int MINIMUM_NB_PERSON = 1;

	public BearObservation(CottageBase component, int nbPerson) {
		super(component, DEFAULT_UNIT_PRICE, nbPerson);
	}

	@Override
	protected void validateNbPerson(int nbPerson) {
		if(nbPerson < MINIMUM_NB_PERSON) throw new InvalidPersonNumberException();
	}
}
