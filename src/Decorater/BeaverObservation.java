package Decorater;

import Cottage.CottageBase;
import exceptions.InvalidPersonNumberException;

public class BeaverObservation extends IndividualServices {
	public static final double DEFAULT_UNIT_PRICE = 8;
	public static final int MINIMUM_NB_PERSON = 0;

	public BeaverObservation(CottageBase component, int nbPerson) {
		super(component, DEFAULT_UNIT_PRICE, nbPerson);
	}

	@Override
	protected void validateNbPerson(int nbPerson) {
		if(nbPerson < MINIMUM_NB_PERSON) throw new InvalidPersonNumberException();
		
	}
	
	
}
