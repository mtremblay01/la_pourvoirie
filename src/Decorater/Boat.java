package Decorater;

import Cottage.CottageBase;

public class Boat extends Transport{

	public static final double DEFAULT_UNIT_PRICE = 30;
	
	public Boat(CottageBase component, int noPerson) {
		super(component, DEFAULT_UNIT_PRICE, noPerson);
	}
}
