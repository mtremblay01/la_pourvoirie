package Decorater;

import Cottage.CottageBase;

public class Cleaning extends DailyServices{

	public static final double DEFAULT_UNIT_PRICE = 8;
	
	public Cleaning(CottageBase component, int nbDays) {
		super(component, DEFAULT_UNIT_PRICE, nbDays);
	}
	
}