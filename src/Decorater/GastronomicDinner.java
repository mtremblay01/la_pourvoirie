package Decorater;

import Cottage.CottageBase;

public class GastronomicDinner extends Meals{

	public static final double DEFAULT_UNIT_PRICE = 22;
	
	public GastronomicDinner(CottageBase component, int nbDays, int nbPersons) {
		super(component, DEFAULT_UNIT_PRICE, nbDays, nbPersons);
	}
}
