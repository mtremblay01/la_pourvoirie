package Decorater;

import Cottage.CottageBase;

public class ATV extends DailyServices {

	public static final double DEFAULT_UNIT_PRICE = 30;
	public ATV(CottageBase component, int nbDays) {
		super(component, DEFAULT_UNIT_PRICE, nbDays);
	}
}
