package Decorater;

import Cottage.CottageBase;

public class Dinner extends Meals{
	
	public static final double DEFAULT_UNIT_PRICE = 18.0;
	
	public Dinner(CottageBase component, int nbDays, int nbPersons) {
		super(component, DEFAULT_UNIT_PRICE, nbDays, nbPersons);
	}
}