package Decorater;

import Cottage.CottageBase;

public class HydroAirplane extends Transport{

	public static final double DEFAULT_UNIT_PRICE = 80;
	
	public HydroAirplane(CottageBase component, int noPerson) {
		super(component, DEFAULT_UNIT_PRICE, noPerson);
	}
}
