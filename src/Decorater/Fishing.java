package Decorater;

import Cottage.CottageBase;
import exceptions.InvalidPersonNumberException;

public class Fishing extends IndividualServices {

	public static final double DEFAULT_UNIT_PRICE = 30;
	public static final int MINIMUM_PERSON = 4;
	public static final int MAXIMUM_PERSON = 10;
	
	public Fishing(CottageBase component, int nbPerson) {
		super(component, DEFAULT_UNIT_PRICE, nbPerson);
	}

	@Override
	protected void validateNbPerson(int nbPerson) {
		if(nbPerson < MINIMUM_PERSON || nbPerson > MAXIMUM_PERSON) throw new InvalidPersonNumberException();
	}
}
