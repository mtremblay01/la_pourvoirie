package Decorater;

import Cottage.CottageBase;

public class BreakfastAndLunch extends Meals{
	
	public static final double DEFAULT_UNIT_PRICE = 10.0;

	public BreakfastAndLunch(CottageBase component, int nbDays, int nbPersons) {
		super(component, DEFAULT_UNIT_PRICE, nbDays, nbPersons);
	}
}
