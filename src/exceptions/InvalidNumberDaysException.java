package exceptions;
public class InvalidNumberDaysException extends IllegalArgumentException{
	private static String ERROR = "Invalid Day Number";
	public InvalidNumberDaysException() {
		super(ERROR);
	}
}
 