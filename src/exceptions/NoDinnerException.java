package exceptions;
public class NoDinnerException extends IllegalArgumentException{
	private static String ERROR = "No Dinner";
	public NoDinnerException() {
		super(ERROR);
	}
}
 