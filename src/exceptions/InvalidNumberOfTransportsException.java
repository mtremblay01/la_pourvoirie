package exceptions;
public class InvalidNumberOfTransportsException extends IllegalArgumentException{
	private static String ERROR = "Invalid Transport Number";
	public InvalidNumberOfTransportsException() {
		super(ERROR);
	}
}
 