package exceptions;
public class AlreadySelectedDinnerException extends IllegalArgumentException{
	private static String ERROR = "Already Selected Dinner";
	public AlreadySelectedDinnerException() {
		super(ERROR);
	}
}
 