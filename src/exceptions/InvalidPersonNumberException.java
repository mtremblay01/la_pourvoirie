package exceptions;
public class InvalidPersonNumberException extends IllegalArgumentException{
	private static String ERROR = "must be between 4 and 10 person";
	public InvalidPersonNumberException() {
		super(ERROR);
	}
}
 