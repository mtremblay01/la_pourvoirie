package Cottage;

public class CottageMedium extends Cottage {
	public static final double UNIT_PRICE = 200;
	public static final int MAXIMUM_PERSON = 6;
	
	public CottageMedium(int days) {
		super(days, UNIT_PRICE);
	}
}
