package Cottage;

public class CottageSmall extends Cottage {
	public static final double UNIT_PRICE = 150;
	public static final int MAXIMUM_PERSON = 4;
	
	public CottageSmall(int days) {
		super(days, UNIT_PRICE);
	}
	
}
