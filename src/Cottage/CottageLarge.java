package Cottage;

public class CottageLarge extends Cottage {
	public static final double UNIT_PRICE = 300;
	public static final int MAXIMUM_PERSON = 10;
	public CottageLarge(int days) {
		super(days, UNIT_PRICE);
	}
}
