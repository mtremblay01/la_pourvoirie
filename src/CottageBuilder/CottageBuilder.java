package CottageBuilder;

import Decorater.ATV;
import Cottage.CottageBase;
import Cottage.CottageLarge;
import Cottage.CottageMedium;
import Cottage.CottageSmall;
import Decorater.*;
import exceptions.AlreadySelectedDinnerException;
import exceptions.InvalidNumberDaysException;
import exceptions.InvalidNumberOfTransportsException;
import exceptions.InvalidPersonNumberException;
import exceptions.NoDinnerException;

public class CottageBuilder {
	public enum CottageSize {
		SMALL,
		MEDIUM,
		LARGE
	}
	
	public static final int REQUIRED_NB_TRANSPORT = 2;

	private CottageBase component;
	private int nbDays;
	private int nbPerson;
	private int nbTransport;
	private boolean hasDinner;
	
	public CottageBuilder buildCottage(CottageSize size, int nbDays, int nbPerson) {
		
		switch (size) {
		case SMALL:
			if (nbPerson > CottageSmall.MAXIMUM_PERSON) throw new InvalidPersonNumberException();
			this.component = new CottageSmall(nbDays);
			break;
		case MEDIUM:
			if (nbPerson > CottageMedium.MAXIMUM_PERSON) throw new InvalidPersonNumberException();
			this.component = new CottageMedium(nbDays);
			break;
		case LARGE:
			if (nbPerson > CottageLarge.MAXIMUM_PERSON) throw new InvalidPersonNumberException();
			this.component = new CottageLarge(nbDays);
			break;
		}
		
		this.nbDays = nbDays;
		this.nbPerson = nbPerson;
		this.nbTransport = 0;
		this.hasDinner = false;
		
		return this;
	}
	
	public CottageBase done() {
		if (this.nbTransport != REQUIRED_NB_TRANSPORT) throw new InvalidNumberOfTransportsException();
		if (!this.hasDinner) throw new NoDinnerException();
		return this.component;
	}
	
	public CottageBuilder withATV(int nbDays) {
		if (nbDays > this.nbDays) throw new InvalidNumberDaysException();
		this.component = new ATV(this.component, nbDays);
		return this;
	}
	
	public CottageBuilder withCleaningService(int nbDays) {
		if (nbDays > this.nbDays) throw new InvalidNumberDaysException();
		this.component = new Cleaning(this.component, nbDays);
		return this;
	}
	
	public CottageBuilder withBearObservation(int nbPerson) {
		if (nbPerson > this.nbPerson) throw new InvalidPersonNumberException();
		this.component = new BearObservation(this.component, nbPerson);
		return this;
	}
	
	public CottageBuilder withBeaverObservation(int nbPerson) {
		if (nbPerson > this.nbPerson) throw new InvalidPersonNumberException();
		this.component = new BeaverObservation(this.component, nbPerson);
		return this;
	}
	
	public CottageBuilder withWolfObservation(int nbPerson) {
		if (nbPerson > this.nbPerson) throw new InvalidPersonNumberException();
		this.component = new WolfObservation(this.component, nbPerson);
		return this;
	}
	
	public CottageBuilder withFishing(int nbPerson) {
		if (nbPerson > this.nbPerson) throw new InvalidPersonNumberException();
		this.component = new Fishing(this.component, nbPerson);
		return this;
	}
	
	public CottageBuilder withHunting(int nbPerson) {
		if (nbPerson > this.nbPerson) throw new InvalidPersonNumberException();
		this.component = new Hunting(this.component, nbPerson);
		return this;
	}
	
	public CottageBuilder withFlyCrafting(int nbPerson) {
		if (nbPerson > this.nbPerson) throw new InvalidPersonNumberException();
		this.component = new FlyCrafting(this.component, nbPerson);
		return this;
	}
	
	public CottageBuilder withFlyFishing(int nbPerson) {
		if (nbPerson > this.nbPerson) throw new InvalidPersonNumberException();
		this.component = new FlyFishing(this.component, nbPerson);
		return this;
	}
	
	public CottageBuilder withTrapping(int nbPerson) {
		if (nbPerson > this.nbPerson) throw new InvalidPersonNumberException();
		this.component = new Trapping(this.component, nbPerson);
		return this;
	}
	
	public CottageBuilder withBreakfastAndLunch(int nbPerson, int nbDays) {
		if (nbPerson > this.nbPerson) throw new InvalidPersonNumberException();
		if (nbDays > this.nbDays) throw new InvalidNumberDaysException();
		this.component = new BreakfastAndLunch(this.component, nbDays, nbPerson);
		return this;
	}
	
	public CottageBuilder withDinner() {
		if (this.hasDinner) throw new AlreadySelectedDinnerException();
		this.component = new Dinner(this.component, this.nbDays, this.nbPerson);
		this.hasDinner = true;
		return this;
	}
	
	public CottageBuilder withGastronomicDinner(int nbPerson, int nbDays) {
		if (nbPerson > this.nbPerson) throw new InvalidPersonNumberException();
		if (nbDays > this.nbDays) throw new InvalidNumberDaysException();
		this.component = new GastronomicDinner(this.component, nbDays, nbPerson);
		return this;
	}
	
	public CottageBuilder withBoat() {
		this.component = new Boat(this.component, this.nbPerson); 
		this.nbTransport++;
		return this;
	}
	
	public CottageBuilder withHydroAirplane() {
		this.component = new HydroAirplane(this.component, this.nbPerson);
		this.nbTransport++;
		return this;
	}
}
